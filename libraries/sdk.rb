module IntelliJ
  module Workspace
    module SDK
      def generate_roots_config(type, path)
        case type.to_sym
        when :JavaSDK
          <<-EOH
            <roots>
              <annotationsPath>
                <root type="composite">
                  <root url="jar://$APPLICATION_HOME_DIR$/lib/jdkAnnotations.jar!/" type="simple" />
                </root>
              </annotationsPath>
              <classPath>
                <root type="composite">
                  <root url="jrt://#{path}!/java.activation" type="simple" />
                  <root url="jrt://#{path}!/java.base" type="simple" />
                  <root url="jrt://#{path}!/java.compiler" type="simple" />
                  <root url="jrt://#{path}!/java.corba" type="simple" />
                  <root url="jrt://#{path}!/java.datatransfer" type="simple" />
                  <root url="jrt://#{path}!/java.desktop" type="simple" />
                  <root url="jrt://#{path}!/java.instrument" type="simple" />
                  <root url="jrt://#{path}!/java.jnlp" type="simple" />
                  <root url="jrt://#{path}!/java.logging" type="simple" />
                  <root url="jrt://#{path}!/java.management" type="simple" />
                  <root url="jrt://#{path}!/java.management.rmi" type="simple" />
                  <root url="jrt://#{path}!/java.naming" type="simple" />
                  <root url="jrt://#{path}!/java.prefs" type="simple" />
                  <root url="jrt://#{path}!/java.rmi" type="simple" />
                  <root url="jrt://#{path}!/java.scripting" type="simple" />
                  <root url="jrt://#{path}!/java.se" type="simple" />
                  <root url="jrt://#{path}!/java.se.ee" type="simple" />
                  <root url="jrt://#{path}!/java.security.jgss" type="simple" />
                  <root url="jrt://#{path}!/java.security.sasl" type="simple" />
                  <root url="jrt://#{path}!/java.smartcardio" type="simple" />
                  <root url="jrt://#{path}!/java.sql" type="simple" />
                  <root url="jrt://#{path}!/java.sql.rowset" type="simple" />
                  <root url="jrt://#{path}!/java.transaction" type="simple" />
                  <root url="jrt://#{path}!/java.xml" type="simple" />
                  <root url="jrt://#{path}!/java.xml.bind" type="simple" />
                  <root url="jrt://#{path}!/java.xml.crypto" type="simple" />
                  <root url="jrt://#{path}!/java.xml.ws" type="simple" />
                  <root url="jrt://#{path}!/java.xml.ws.annotation" type="simple" />
                  <root url="jrt://#{path}!/javafx.base" type="simple" />
                  <root url="jrt://#{path}!/javafx.controls" type="simple" />
                  <root url="jrt://#{path}!/javafx.deploy" type="simple" />
                  <root url="jrt://#{path}!/javafx.fxml" type="simple" />
                  <root url="jrt://#{path}!/javafx.graphics" type="simple" />
                  <root url="jrt://#{path}!/javafx.media" type="simple" />
                  <root url="jrt://#{path}!/javafx.swing" type="simple" />
                  <root url="jrt://#{path}!/javafx.web" type="simple" />
                  <root url="jrt://#{path}!/jdk.accessibility" type="simple" />
                  <root url="jrt://#{path}!/jdk.aot" type="simple" />
                  <root url="jrt://#{path}!/jdk.attach" type="simple" />
                  <root url="jrt://#{path}!/jdk.charsets" type="simple" />
                  <root url="jrt://#{path}!/jdk.compiler" type="simple" />
                  <root url="jrt://#{path}!/jdk.crypto.cryptoki" type="simple" />
                  <root url="jrt://#{path}!/jdk.crypto.ec" type="simple" />
                  <root url="jrt://#{path}!/jdk.crypto.mscapi" type="simple" />
                  <root url="jrt://#{path}!/jdk.deploy" type="simple" />
                  <root url="jrt://#{path}!/jdk.deploy.controlpanel" type="simple" />
                  <root url="jrt://#{path}!/jdk.dynalink" type="simple" />
                  <root url="jrt://#{path}!/jdk.editpad" type="simple" />
                  <root url="jrt://#{path}!/jdk.hotspot.agent" type="simple" />
                  <root url="jrt://#{path}!/jdk.httpserver" type="simple" />
                  <root url="jrt://#{path}!/jdk.incubator.httpclient" type="simple" />
                  <root url="jrt://#{path}!/jdk.internal.ed" type="simple" />
                  <root url="jrt://#{path}!/jdk.internal.jvmstat" type="simple" />
                  <root url="jrt://#{path}!/jdk.internal.le" type="simple" />
                  <root url="jrt://#{path}!/jdk.internal.opt" type="simple" />
                  <root url="jrt://#{path}!/jdk.internal.vm.ci" type="simple" />
                  <root url="jrt://#{path}!/jdk.internal.vm.compiler" type="simple" />
                  <root url="jrt://#{path}!/jdk.internal.vm.compiler.management" type="simple" />
                  <root url="jrt://#{path}!/jdk.jartool" type="simple" />
                  <root url="jrt://#{path}!/jdk.javadoc" type="simple" />
                  <root url="jrt://#{path}!/jdk.javaws" type="simple" />
                  <root url="jrt://#{path}!/jdk.jcmd" type="simple" />
                  <root url="jrt://#{path}!/jdk.jconsole" type="simple" />
                  <root url="jrt://#{path}!/jdk.jdeps" type="simple" />
                  <root url="jrt://#{path}!/jdk.jdi" type="simple" />
                  <root url="jrt://#{path}!/jdk.jdwp.agent" type="simple" />
                  <root url="jrt://#{path}!/jdk.jfr" type="simple" />
                  <root url="jrt://#{path}!/jdk.jlink" type="simple" />
                  <root url="jrt://#{path}!/jdk.jshell" type="simple" />
                  <root url="jrt://#{path}!/jdk.jsobject" type="simple" />
                  <root url="jrt://#{path}!/jdk.jstatd" type="simple" />
                  <root url="jrt://#{path}!/jdk.localedata" type="simple" />
                  <root url="jrt://#{path}!/jdk.management" type="simple" />
                  <root url="jrt://#{path}!/jdk.management.agent" type="simple" />
                  <root url="jrt://#{path}!/jdk.management.cmm" type="simple" />
                  <root url="jrt://#{path}!/jdk.management.jfr" type="simple" />
                  <root url="jrt://#{path}!/jdk.management.resource" type="simple" />
                  <root url="jrt://#{path}!/jdk.naming.dns" type="simple" />
                  <root url="jrt://#{path}!/jdk.naming.rmi" type="simple" />
                  <root url="jrt://#{path}!/jdk.net" type="simple" />
                  <root url="jrt://#{path}!/jdk.pack" type="simple" />
                  <root url="jrt://#{path}!/jdk.packager" type="simple" />
                  <root url="jrt://#{path}!/jdk.packager.services" type="simple" />
                  <root url="jrt://#{path}!/jdk.plugin" type="simple" />
                  <root url="jrt://#{path}!/jdk.plugin.server" type="simple" />
                  <root url="jrt://#{path}!/jdk.rmic" type="simple" />
                  <root url="jrt://#{path}!/jdk.scripting.nashorn" type="simple" />
                  <root url="jrt://#{path}!/jdk.scripting.nashorn.shell" type="simple" />
                  <root url="jrt://#{path}!/jdk.sctp" type="simple" />
                  <root url="jrt://#{path}!/jdk.security.auth" type="simple" />
                  <root url="jrt://#{path}!/jdk.security.jgss" type="simple" />
                  <root url="jrt://#{path}!/jdk.snmp" type="simple" />
                  <root url="jrt://#{path}!/jdk.unsupported" type="simple" />
                  <root url="jrt://#{path}!/jdk.xml.bind" type="simple" />
                  <root url="jrt://#{path}!/jdk.xml.dom" type="simple" />
                  <root url="jrt://#{path}!/jdk.xml.ws" type="simple" />
                  <root url="jrt://#{path}!/jdk.zipfs" type="simple" />
                  <root url="jrt://#{path}!/oracle.desktop" type="simple" />
                  <root url="jrt://#{path}!/oracle.net" type="simple" />
                </root>
              </classPath>
              <javadocPath>
                <root type="composite" />
              </javadocPath>
              <sourcePath>
                <root type="composite">
                  <root url="jar://#{path}/lib/src.zip!/java.se" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.aot" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.jdi" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.net" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/java.rmi" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/java.sql" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/java.xml" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.jcmd" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.pack" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.rmic" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.sctp" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/java.base" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.jdeps" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.jlink" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.zipfs" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/java.corba" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/java.prefs" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/java.se.ee" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/javafx.web" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.attach" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.jshell" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.jstatd" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.xml.ws" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/java.naming" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/java.xml.ws" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/javafx.base" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/javafx.fxml" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.editpad" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.jartool" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.javadoc" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.xml.dom" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/java.desktop" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/java.logging" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/javafx.media" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/javafx.swing" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.charsets" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.compiler" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.dynalink" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.jconsole" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.jsobject" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.packager" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.xml.bind" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/java.compiler" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/java.xml.bind" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.crypto.ec" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/java.scripting" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.httpserver" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.jdwp.agent" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.localedata" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.management" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.naming.dns" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.naming.rmi" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/java.activation" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/java.instrument" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/java.management" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/java.sql.rowset" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/java.xml.crypto" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/javafx.controls" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/javafx.graphics" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.internal.ed" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.internal.le" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.unsupported" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/java.smartcardio" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/java.transaction" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.internal.opt" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/java.datatransfer" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.accessibility" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.crypto.mscapi" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.hotspot.agent" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.security.auth" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.security.jgss" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/java.security.jgss" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/java.security.sasl" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.internal.vm.ci" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/java.management.rmi" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.crypto.cryptoki" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.internal.jvmstat" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.management.agent" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.packager.services" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.scripting.nashorn" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/java.xml.ws.annotation" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.incubator.httpclient" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.internal.vm.compiler" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.scripting.nashorn.shell" type="simple" />
                  <root url="jar://#{path}/lib/src.zip!/jdk.internal.vm.compiler.management" type="simple" />
                </root>
              </sourcePath>
            </roots>
            <additional />
          EOH
        when :RUBY_SDK
          <<-EOH
            <roots>
              <annotationsPath>
                <root type="composite" />
              </annotationsPath>
              <classPath>
                <root type="composite">
                  <root url="file://#{path}/lib/ruby/gems/2.6.0/gems/did_you_mean-1.3.0/lib" type="simple" />
                  <root url="file://#{path}/lib/ruby/site_ruby/2.6.0" type="simple" />
                  <root url="file://#{path}/lib/ruby/site_ruby" type="simple" />
                  <root url="file://#{path}/lib/ruby/vendor_ruby/2.6.0" type="simple" />
                  <root url="file://#{path}/lib/ruby/vendor_ruby/2.6.0/x64-msvcrt" type="simple" />
                  <root url="file://#{path}/lib/ruby/vendor_ruby" type="simple" />
                  <root url="file://#{path}/lib/ruby/2.6.0" type="simple" />
                  <root url="file://#{path}/lib/ruby/2.6.0/x64-mingw32" type="simple" />
                  <root url="file://$APPLICATION_PLUGINS_DIR$/ruby/rubystubs/rubystubs26" type="simple" />
                </root>
              </classPath>
              <javadocPath>
                <root type="composite" />
              </javadocPath>
              <sourcePath>
                <root type="composite">
                  <root url="file://#{path}/lib/ruby/gems/2.6.0/gems/did_you_mean-1.3.0/lib" type="simple" />
                  <root url="file://#{path}/lib/ruby/site_ruby/2.6.0" type="simple" />
                  <root url="file://#{path}/lib/ruby/site_ruby" type="simple" />
                  <root url="file://#{path}/lib/ruby/vendor_ruby/2.6.0" type="simple" />
                  <root url="file://#{path}/lib/ruby/vendor_ruby/2.6.0/x64-msvcrt" type="simple" />
                  <root url="file://#{path}/lib/ruby/vendor_ruby" type="simple" />
                  <root url="file://#{path}/lib/ruby/2.6.0" type="simple" />
                  <root url="file://#{path}/lib/ruby/2.6.0/x64-mingw32" type="simple" />
                  <root url="file://$APPLICATION_PLUGINS_DIR$/ruby/rubystubs/rubystubs26" type="simple" />
                </root>
              </sourcePath>
            </roots>
            <additional NORMALIZED_VERSION_STRING="ver.2.6.1p33 ( revision 66950) p33" PLATFORM="x64-mingw32" GEMS_BIN_DIR_PATH="#{path}/bin">
              <VERSION_MANAGER ID="system" />
            </additional>
          EOH
        end
      end
    end
  end
end
