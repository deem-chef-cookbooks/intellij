include IntelliJ::API

module IntelliJ
  module Plugins
    def self.included(base)
      base.extend self
    end

    def plugin_data_from_repository(name, edition, update, intellij_path)
      intellij_build = nil

      if intellij_path
        _, edition_code, build_number = local_intellij_info(intellij_path)
        edition = edition_from_code(edition_code)
        intellij_build = build_number
      end

      plugin = find_plugin(name, edition)
      update = get_download_link(plugin, update, intellij_build)
      update['edition'] = edition
      update['intellij_build'] = intellij_build
      update
    end

    def find_plugin(name, edition)
      suggestions = suggest_plugin(name, edition)
      raise IntelliJ::Exceptions::IntelliJPluginNotFound.new(name, edition, suggestions.keys) unless suggestions[name]
      suggestions[name]
    end

    def get_download_link(plugin, update, intellij_build)
      updates = plugin_updates(plugin['id'], plugin['family'])
      update ||= latest_plugin_for_build(updates, intellij_build, plugin['name'])

      raise IntelliJ::Exceptions::IntelliJPluginVersionNotFound.new(plugin['name'], update, updates.keys) unless updates[update.to_i]
      updates[update.to_i]
    end

    def plugin_installed?(repository, source, repository_filename)
      repo_name = repository_filename || ::File.basename(URI.parse(source).path)
      ::File.exist?("#{repository}/#{repo_name}")
    end

    def latest_plugin_for_build(updates, build_number, plugin_name)
      matching = []

      unless build_number.to_s.empty?
        Chef::Log.debug("Finding updates matching build: #{build_number}. Updates:\n#{updates}")

        updates.each do |id, update|
          is_valid = version_earlier?(update['since'], build_number) && version_earlier?(build_number, update['until'])
          matching.push(id) if is_valid
        end

        Chef::Log.warn("Could not find any plugin '#{plugin_name}' update for the IntelliJ #{build_number}! Most likely IntelliJ will ask you to update the plugin after the start. Using the latest (#{updates.keys[0]}) update.") if matching.empty?
      end

      matching.max || updates.keys[0]
    end
  end
end
