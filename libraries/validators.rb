module IntelliJ
  module Validators
    def self.all_keys_in?(allowed, map)
      map.each do |_, parameters|
        parameters.each do |key, _|
          return false unless allowed.include?(key.to_s)
        end
      end
      true
    end

    module DockerIntegration
      def self.registry_address_defined?
        lambda do |registries|
          registries.each do |_, parameters|
            return false unless parameters&.key?('address') || parameters&.key?(:address)
          end
        end
      end

      def self.proper_registry_keys?
        lambda do |p|
          IntelliJ::Validators.all_keys_in?(%w(address email username), p)
        end
      end
    end
  end
end
