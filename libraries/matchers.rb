if defined?(ChefSpec)
  def update_intellij(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij, :update, resource_name)
  end

  def install_intellij(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij, :install, resource_name)
  end

  def delete_intellij(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij, :delete, resource_name)
  end

  def create_intellij_artifact(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij_artifact, :create, resource_name)
  end

  def delete_intellij_artifact(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij_artifact, :delete, resource_name)
  end

  def create_intellij_changelist(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij_changelist, :create, resource_name)
  end

  def delete_intellij_changelist(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij_changelist, :delete, resource_name)
  end

  def update_intellij_changelist(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij_changelist, :update, resource_name)
  end

  def add_files_to_intellij_changelist(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij_changelist, :add_files, resource_name)
  end

  def delete_files_from_intellij_changelist(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij_changelist, :delete_files, resource_name)
  end

  def create_intellij_module(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij_module, :create, resource_name)
  end

  def delete_intellij_module(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij_module, :delete, resource_name)
  end

  def install_intellij_plugin(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij_plugin, :install, resource_name)
  end

  def update_intellij_plugin(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij_update, :install, resource_name)
  end

  def create_intellij_project(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij_project, :create, resource_name)
  end

  def delete_intellij_project(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij_project, :delete, resource_name)
  end

  def create_intellij_run_configuration(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij_run_configuration, :create, resource_name)
  end

  def delete_intellij_run_configuration(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij_run_configuration, :delete, resource_name)
  end

  def add_intellij_sdk(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij_sdk, :add, resource_name)
  end

  def delete_intellij_sdk(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij_sdk, :delete, resource_name)
  end

  def create_intellij_workspace(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij_workspace, :create, resource_name)
  end

  def delete_intellij_workspace(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij_workspace, :delete, resource_name)
  end

  def create_intellij_docker_integration(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij_docker_integration, :create, resource_name)
  end

  def merge_intellij_docker_integration(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij_docker_integration, :merge, resource_name)
  end

  def delete_intellij_docker_integration(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij_docker_integration, :delete, resource_name)
  end

  def create_intellij_application_server(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij_application_server, :create, resource_name)
  end

  def delete_intellij_application_server(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij_application_server, :delete, resource_name)
  end

  def create_intellij_shortcut(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij_shortcut, :create, resource_name)
  end

  def delete_intellij_shortcut(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:intellij_shortcut, :delete, resource_name)
  end
end
