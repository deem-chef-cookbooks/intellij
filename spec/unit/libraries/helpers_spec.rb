require 'spec_helper'
require_relative '../../../../intellij/libraries/.helpers'
require_relative '../../../../intellij/libraries/.jetbrains.api.rb'

describe 'Testing the helpers library' do
  before do
    class HelpersLibrary
      include IntelliJ::Helpers
    end
  end

  context 'testing dropping last elements of the array' do
    it 'returns original array if the amount is 0' do
      array = [0, 1, 2, 3, 4]

      result = HelpersLibrary.drop_last_elements(array, 0)

      expect(result).to eq(array)
    end

    it 'shortens the array if the amount is not 0' do
      array = [0, 1, 2, 3, 4]

      result = HelpersLibrary.drop_last_elements(array, 2)

      expect(result).to eq([0, 1, 2])
    end

    it 'clears the array if the amount is greater than its length' do
      array = [0, 1, 2, 3, 4]

      result = HelpersLibrary.drop_last_elements(array, 10)

      expect(result).to eq([])
    end
  end

  context 'testing the versions normalisation' do
    it 'does nothing if the versions are specified fully' do
      first = '10.0.1'
      second = '10.1.2'

      f_result, s_result = HelpersLibrary.normalize_versions(first, second)

      expect(f_result).to eq(first)
      expect(s_result).to eq(second)
    end

    it 'normalises both versions to unified format if at least one is defined with *' do
      first = '10.0.*'
      second = '10.1.2'

      f_result, s_result = HelpersLibrary.normalize_versions(first, second)

      expect(f_result).to eq('10.0')
      expect(s_result).to eq('10.1')
    end

    it 'normalises both versions to minimal format if the * are defined in different places' do
      first = '10.0.*'
      second = '10.*'

      f_result, s_result = HelpersLibrary.normalize_versions(first, second)

      expect(f_result).to eq('10')
      expect(s_result).to eq('10')
    end

    it 'should be resistant to weird format with versions after the *' do
      first = '10.0.*'
      second = '10.1.*.11.*'

      f_result, s_result = HelpersLibrary.normalize_versions(first, second)

      expect(f_result).to eq('10.0')
      expect(s_result).to eq('10.1')
    end

    it 'should return empty strings if the first field is *' do
      first = '10.0.*'
      second = '*'

      f_result, s_result = HelpersLibrary.normalize_versions(first, second)

      expect(f_result).to be_empty
      expect(s_result).to be_empty
    end
  end

  context 'testing the extended versions comparision' do
    it 'detects that the version is smaller with normal format' do
      first = '10.0.1'
      second = '10.1.2'

      result = HelpersLibrary.version_earlier?(first, second)

      expect(result).to be true
    end

    it 'detects that the version is bigger with normal format' do
      first = '10.1.2'
      second = '10.0.1'

      result = HelpersLibrary.version_earlier?(first, second)

      expect(result).to be false
    end

    it 'correctly manages the extended format comparision' do
      first = '10.0.*'
      second = '10.0.1'

      result = HelpersLibrary.version_earlier?(first, second)

      expect(result).to be true
    end
  end
end
