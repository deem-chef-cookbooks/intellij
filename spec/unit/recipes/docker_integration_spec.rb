require 'spec_helper'

describe 'intellij_docker_integration' do
  describe 'testing the resource' do
    step_into :intellij_docker_integration
    platform 'ubuntu'

    context 'creating the basic configuration' do
      before do
        allow(File).to receive(:read).with(anything).and_call_original
        allow(File).to receive(:read).with('/home/user/myworkspace/config/options/remote-servers.xml').and_return '<application/>'
      end

      recipe do
        intellij_docker_integration 'Docker' do
          workspace_path '/home/user/myworkspace'
          user 'myuser'
          group 'mygroup'
        end
      end

      it { is_expected.to create_directory('/home/user/myworkspace').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_directory('/home/user/myworkspace/config/options').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_if_missing_file('/home/user/myworkspace/config/options/remote-servers.xml').with(user: 'myuser', group: 'mygroup') }
      it do
        is_expected.to append_if_missing_xml('/home/user/myworkspace/config/options/remote-servers.xml')
          .with(target: '/application/component[@name="RemoteServers"]/remote-server[@name="Docker"][@type="docker"]')
      end
      it { is_expected.to_not create_if_missing_file('/home/user/myworkspace/config/options/docker-registry.xml') }
      it { is_expected.to_not bulk_edit_xml('/home/user/myworkspace/config/options/docker-registry.xml') }
    end

    context 'creating the advanced configuration' do
      before do
        allow(File).to receive(:read).with(anything).and_call_original
        allow(File).to receive(:read).with('/home/user/myworkspace/config/options/remote-servers.xml').and_return '<application/>'
        allow(File).to receive(:read).with('/home/user/myworkspace/config/options/docker-registry.xml').and_return '<application/>'
      end

      recipe do
        intellij_docker_integration 'Docker' do
          workspace_path '/home/user/myworkspace'
          user 'myuser'
          group 'mygroup'
          registries(
            'Docker Registry': {
              address: 'registry.hub.docker.com',
              username: 'myuser',
              email: 'myuser@mycompany.com',
            }
          )
          shared_directories(
            '/home/myuser/shared': '/app/shared'
          )
        end
      end

      it { is_expected.to create_directory('/home/user/myworkspace').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_directory('/home/user/myworkspace/config/options').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_if_missing_file('/home/user/myworkspace/config/options/remote-servers.xml').with(user: 'myuser', group: 'mygroup') }
      it do
        is_expected.to append_if_missing_xml('/home/user/myworkspace/config/options/remote-servers.xml')
          .with(target: '/application/component[@name="RemoteServers"]/remote-server[@name="Docker"][@type="docker"]')
      end
      it { is_expected.to create_if_missing_file('/home/user/myworkspace/config/options/docker-registry.xml').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to bulk_edit_xml('/home/user/myworkspace/config/options/docker-registry.xml') }
    end

    context 'merging with non-existing configuration' do
      before do
        allow(File).to receive(:exist?).with(anything).and_call_original
        allow(File).to receive(:exist?).with('/home/user/myworkspace/config/options/remote-servers.xml').and_return false
        allow(File).to receive(:exist?).with('/home/user/myworkspace/config/options/docker-registry.xml').and_return false
      end

      recipe do
        intellij_docker_integration 'Docker' do
          workspace_path '/home/user/myworkspace'
          registries(
            'Docker Registry': {
              address: 'registry.hub.docker.com',
            }
          )
          shared_directories(
            '/home/dir': '/test/dir'
          )
          user 'myuser'
          group 'mygroup'
          action :merge
        end
      end

      it do
        is_expected.to create_intellij_docker_integration('Docker')
          .with(
            user: 'myuser',
            group: 'mygroup',
            workspace_path: '/home/user/myworkspace',
            registries: {
              'Docker Registry': {
                address: 'registry.hub.docker.com',
              },
            },
            shared_directories: {
              '/home/dir': '/test/dir',
            }
          )
      end
    end

    context 'merging with existing configuration' do
      before do
        allow(File).to receive(:exist?).with(anything).and_call_original
        allow(File).to receive(:exist?).with('/home/user/myworkspace/config/options/remote-servers.xml').and_return true
        allow(File).to receive(:exist?).with('/home/user/myworkspace/config/options/docker-registry.xml').and_return true
        allow(File).to receive(:read).with(anything).and_call_original
        allow(File).to receive(:read).with('/home/user/myworkspace/config/options/remote-servers.xml').and_return <<-EOH
          <application>
            <component name="RemoteServers">
              <remote-server name="Docker" type="docker">
                <configuration>
                  <option name="apiUrl" value="tcp://localhost:2375" />
                  <option name="pathMappings">
                    <list>
                      <DockerPathMappingImpl>
                        <option name="localPath" value="/home/dir" />
                        <option name="virtualMachinePath" value="/test/dir" />
                      </DockerPathMappingImpl>
                    </list>
                  </option>
                </configuration>
              </remote-server>
            </component>
          </application>
        EOH
        allow(File).to receive(:read).with('/home/user/myworkspace/config/options/docker-registry.xml').and_return <<-EOH
          <application>
            <component name="DockerRegistry">
              <DockerRegistry email="myemail@gmail.com">
                <option name="address" value="registry.hub.docker.com" />
                <option name="name" value="Docker Registry" />
                <option name="username" value="thedeem" />
              </DockerRegistry>
            </component>
          </application>
        EOH
      end

      recipe do
        intellij_docker_integration 'Docker' do
          workspace_path '/home/user/myworkspace'
          registries(
            'Docker Registry': {
              address: 'registry.hub.docker.com2',
            },
            'Another Registry': {
              address: 'another.registry.com',
              email: 'myanother@email.com',
              username: 'test',
            }
          )
          shared_directories(
            '/home/other/dir': '/test/dir'
          )
          action :merge
        end
      end

      it do
        is_expected.to create_intellij_docker_integration('Docker')
          .with(
            workspace_path: '/home/user/myworkspace',
            registries: {
              'Docker Registry': {
                address: 'registry.hub.docker.com2',
              },
              'Another Registry': {
                address: 'another.registry.com',
                email: 'myanother@email.com',
                username: 'test',
              },
            },
            shared_directories: {
              '/home/dir': '/test/dir',
              '/home/other/dir': '/test/dir',
            }
          )
      end
    end

    context 'deleting the docker integration' do
      before do
        allow(File).to receive(:exist?).with(anything).and_call_original
        allow(File).to receive(:exist?).with('/home/user/myworkspace/config/options/remote-servers.xml').and_return true
        allow(File).to receive(:exist?).with('/home/user/myworkspace/config/options/docker-registry.xml').and_return true
      end

      recipe do
        intellij_docker_integration 'Docker' do
          workspace_path '/home/user/myworkspace'
          registries(
            'Docker Registry': {
              address: 'registry.hub.docker.com',
              username: 'myuser',
              email: 'myuser@mycompany.com',
            }
          )
          action :delete
        end
      end

      it { is_expected.to remove_xml('/home/user/myworkspace/config/options/remote-servers.xml') }
      it { is_expected.to bulk_edit_xml('/home/user/myworkspace/config/options/docker-registry.xml') }
    end

    context 'passing invalid registries parameter - missing address' do
      recipe do
        intellij_docker_integration 'Docker' do
          workspace_path '/home/user/myworkspace'
          registries(
            'Docker Registry': {
              username: 'myuser',
              email: 'myuser@mycompany.com',
            }
          )
        end
      end

      it { expect { chef_run }.to raise_error Chef::Exceptions::ValidationFailed, /should have at least the address defined/ }
    end

    context 'passing invalid registries parameter - invalid keys' do
      recipe do
        intellij_docker_integration 'Docker' do
          workspace_path '/home/user/myworkspace'
          registries(
            'Docker Registry': {
              address: 'registry.hub.docker.com',
              invalid_key: 'myuser@mycompany.com',
            }
          )
        end
      end

      it { expect { chef_run }.to raise_error Chef::Exceptions::ValidationFailed, /should not have different keys than address, email and username/ }
    end
  end

  describe 'testing the package library' do
    before do
      class PackageLibrary
        include IntelliJ::Install
      end
    end
  end
end
