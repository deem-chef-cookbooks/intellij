require 'spec_helper'
require_relative '../../../../intellij/libraries/workspace'

describe 'intellij_workspace' do
  describe 'testing the resource' do
    step_into :intellij_workspace
    platform 'ubuntu'

    context 'creating the workspace' do
      recipe do
        intellij_workspace '/home/user/myworkspace' do
          default_project '/home/user/myproject'
          intellij_path '/home/myuser/IntelliJ'
          color_scheme :Darcula
          user 'myuser'
          group 'mygroup'
          action :create
        end
      end

      before do
        allow(File).to receive(:read).with(anything).and_call_original
        allow(File).to receive(:read).with('/home/user/myworkspace/config/options/recentProjects.xml').and_return '<application/>'
      end

      it { is_expected.to create_intellij_workspace('/home/user/myworkspace') }
      it { is_expected.to create_directory('/home/user/myworkspace').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_directory('/home/user/myworkspace/config').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_directory('/home/user/myworkspace/config/options').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to merge_java_properties('/home/user/myworkspace/config/idea.properties') }
      it { is_expected.to create_file('/home/user/myworkspace/config/idea.properties').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_if_missing_template('/home/user/myworkspace/config/options/recentProjects.xml').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to bulk_edit_xml('/home/user/myworkspace/config/options/recentProjects.xml') }
      it { is_expected.to create_if_missing_template('/home/user/myworkspace/config/options/colors.scheme.xml').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_if_missing_template('/home/user/myworkspace/config/options/laf.xml').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to append_if_missing_xml('/home/user/myworkspace/config/options/colors.scheme.xml').with(target: '/application/component[@name="EditorColorsManagerImpl"]/global_color_scheme') }
      it { is_expected.to append_if_missing_xml('/home/user/myworkspace/config/options/laf.xml').with(target: '/application/component[@name="LafManager"]/laf') }
    end

    context 'deleting the workspace' do
      recipe do
        intellij_workspace '/home/user/myworkspace' do
          action :delete
        end
      end

      it { is_expected.to delete_intellij_workspace('/home/user/myworkspace') }
      it { is_expected.to delete_directory('/home/user/myworkspace/config') }
      it { is_expected.to delete_directory('/home/user/myworkspace/system') }
    end
  end
end
