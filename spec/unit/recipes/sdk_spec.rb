require 'spec_helper'

describe 'intellij_sdk' do
  describe 'testing the resource' do
    step_into :intellij_sdk
    platform 'ubuntu'

    context 'creating the SDK' do
      recipe do
        intellij_sdk 'JDK 10.0' do
          workspace '/home/user/myworkspace'
          path '/usr/local/jdk10.0.2/bin/java'
          type :JavaSDK
          action :add
          user 'myuser'
          group 'mygroup'
        end
      end

      before do
        allow(File).to receive(:read).with(anything).and_call_original
        allow(File).to receive(:read).with('/home/user/myworkspace/config/options/jdk.table.xml').and_return '<application></application>'
      end

      it { is_expected.to add_intellij_sdk('JDK 10.0') }
      it { is_expected.to create_directory('/home/user/myworkspace/config/options').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_if_missing_template('/home/user/myworkspace/config/options/jdk.table.xml').with(user: 'myuser', group: 'mygroup') }
      it do
        is_expected.to append_if_missing_xml('/home/user/myworkspace/config/options/jdk.table.xml')
          .with(target: '/application/component[@name="ProjectJdkTable"]/jdk[name/@value="JDK 10.0"]')
      end
    end

    context 'deleting the SDK' do
      recipe do
        intellij_sdk 'JDK 10.0' do
          workspace '/home/user/myworkspace'
          action :delete
        end
      end

      before do
        allow(File).to receive(:exist?).with(anything).and_call_original
        allow(File).to receive(:exist?).with('/home/user/myworkspace/config/options/jdk.table.xml').and_return true
      end

      it { is_expected.to delete_intellij_sdk('JDK 10.0') }
      it do
        is_expected.to remove_xml('/home/user/myworkspace/config/options/jdk.table.xml')
          .with(target: '/application/component[@name="ProjectJdkTable"]/jdk[name/@value="JDK 10.0"]')
      end
    end
  end
end
