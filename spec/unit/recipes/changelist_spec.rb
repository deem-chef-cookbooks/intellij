require 'spec_helper'
require_relative '../../../../intellij/libraries/.helpers'
require_relative '../../../../intellij/libraries/changelist'

describe 'intellij_changelist' do
  step_into :intellij_changelist
  platform 'ubuntu'

  # TODO: Find a way to check the edits when bulk editing the XML file
  describe 'testing the resource' do
    context 'creating the changelist' do
      recipe do
        intellij_changelist 'My changelist' do
          project_path '/home/myuser/project'
          comment 'This changelist contains my favourite files'
          files %w(/home/myuser/project/example.file.txt)
          user 'myuser'
          group 'mygroup'
          action :create
        end
      end

      it { is_expected.to create_intellij_changelist('My changelist') }
      it { is_expected.to create_directory('/home/myuser/project/.idea').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_if_missing_file('/home/myuser/project/.idea/workspace.xml').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to bulk_edit_xml('/home/myuser/project/.idea/workspace.xml') }
    end

    context 'updating the changelist' do
      recipe do
        intellij_changelist 'My changelist' do
          project_path '/home/myuser/project'
          comment 'This changelist contains my favourite files'
          files %w(/home/myuser/project/example.file.txt)
          user 'myuser'
          group 'mygroup'
          action :update
        end
      end

      it { is_expected.to update_intellij_changelist('My changelist') }
      it { is_expected.to create_directory('/home/myuser/project/.idea').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_if_missing_file('/home/myuser/project/.idea/workspace.xml').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to bulk_edit_xml('/home/myuser/project/.idea/workspace.xml') }
    end

    context 'deleting the changelist' do
      before do
        allow(File).to receive(:exist?).with(anything).and_call_original
        allow(File).to receive(:exist?).with('/home/myuser/project/.idea/workspace.xml').and_return true
      end

      recipe do
        intellij_changelist 'My changelist' do
          project_path '/home/myuser/project'
          action :delete
        end
      end

      it { is_expected.to delete_intellij_changelist('My changelist') }
      it do
        is_expected.to remove_xml('/home/myuser/project/.idea/workspace.xml')
          .with(target: '/project/component[@name="ChangeListManager"]/list[@name="My changelist"]')
      end
    end

    context 'adding new files to the changelist' do
      recipe do
        intellij_changelist 'My changelist' do
          project_path '/home/myuser/project'
          comment 'This changelist contains my favourite files'
          files %w(/home/myuser/project/example.file.txt)
          user 'myuser'
          group 'mygroup'
          action :add_files
        end
      end

      it { is_expected.to add_files_to_intellij_changelist('My changelist') }
      it { is_expected.to create_directory('/home/myuser/project/.idea').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_if_missing_file('/home/myuser/project/.idea/workspace.xml').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to bulk_edit_xml('/home/myuser/project/.idea/workspace.xml') }
    end

    context 'removing files from the changelist' do
      recipe do
        intellij_changelist 'My changelist' do
          project_path '/home/myuser/project'
          comment 'This changelist contains my favourite files'
          files %w(/home/myuser/project/example.file.txt)
          user 'myuser'
          group 'mygroup'
          action :delete_files
        end
      end

      it { is_expected.to delete_files_from_intellij_changelist('My changelist') }
      it { is_expected.to create_directory('/home/myuser/project/.idea').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_if_missing_file('/home/myuser/project/.idea/workspace.xml').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to bulk_edit_xml('/home/myuser/project/.idea/workspace.xml') }
    end
  end
end
