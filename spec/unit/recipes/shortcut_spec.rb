require 'spec_helper'

describe 'intellij_shortcut' do
  describe 'testing the resource' do
    step_into :intellij_shortcut
    platform 'ubuntu'

    context 'creating the shortcut with global configuration' do
      recipe do
        intellij_shortcut '/home/myuser/IntelliJ' do
          shortcut_name 'IntelliJ'
          directory '/home/myuser/shortcuts'
          user 'myuser'
          group 'mygroup'
          action :create
        end
      end

      it { is_expected.to create_intellij_shortcut('/home/myuser/IntelliJ') }
      it { is_expected.to create_directory('/home/myuser/shortcuts').with(user: 'myuser', group: 'mygroup') }
      # Does not work on Windows hosts
      # it { is_expected.to_not create_windows_shortcut('/home/myuser/shortcuts/IntelliJ.lnk') }
      # it { is_expected.to create_template('/home/myuser/shortcuts/IntelliJ.desktop') }
    end

    context 'creating the shortcut using default properties' do
      let(:shortcut_dir) { Chef::Platform.windows? ? 'Desktop' : '.local/share/applications' }

      before do
        mock_intellij_exists('2018.3.3', 'IU', '/home/myuser/IntelliJ')
        allow(Dir).to receive(:home).with(anything).and_call_original
        allow(Dir).to receive(:home).with('myuser').and_return '/home/myuser'
      end

      recipe do
        intellij_shortcut '/home/myuser/IntelliJ' do
          user 'myuser'
          group 'mygroup'
          action :create
        end
      end

      it { is_expected.to create_intellij_shortcut('/home/myuser/IntelliJ') }
      it { is_expected.to create_directory("/home/myuser/#{shortcut_dir}").with(user: 'myuser', group: 'mygroup') }
      # Does not work on Windows hosts
      # it { is_expected.to_not create_windows_shortcut("C:/Users/myuser/#{shortcut_dir}/IntelliJ IDEA Ultimate edition 2018.3.3.lnk") }
      # it { is_expected.to create_template('/home/myuser/shortcuts/IntelliJ IDEA Ultimate edition 2018.3.3.desktop') }
    end

    context 'creating the shortcut with global configuration' do
      recipe do
        intellij_shortcut '/home/myuser/IntelliJ' do
          shortcut_name 'IntelliJ'
          directory '/home/myuser/shortcuts'
          action :delete
        end
      end

      it { is_expected.to delete_intellij_shortcut('/home/myuser/IntelliJ') }
      # Does not work on Windows hosts
      # it { is_expected.to delete_file('/home/myuser/shortcuts/IntelliJ.desktop') }
      # it { is_expected.to_not delete_file('/home/myuser/shortcuts/IntelliJ.lnk') }
    end
  end
end
