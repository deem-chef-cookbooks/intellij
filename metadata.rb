name 'intellij'
maintainer 'Dorian Krefft'
maintainer_email 'Dorian.Krefft@gmail.com'
license 'Apache-2.0'
description 'Manages the installation and configuration of the IntelliJ IDEA'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version '1.3.1'
chef_version '>= 13.0'

issues_url 'https://gitlab.com/deem-chef-cookbooks/intellij/issues'
source_url 'https://gitlab.com/deem-chef-cookbooks/intellij'

%w( ubuntu windows centos fedora debian ).each do |os|
  supports os
end

depends 'zipfile'
depends 'xmledit'
depends 'tar'
depends 'java_properties'
