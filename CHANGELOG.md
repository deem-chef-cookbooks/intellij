# intellij CHANGELOG

# 1.3.1

- Fixed problem with JetBrains Store API during searching for a plugin (#34).

# 1.3.0

- Fixed a critical error during starting the IntelliJ instance when the changelist resource created an empty change (#31)
- Added new features to the run_configuration resource. It will now allow to select the config as a default one and will
also provide a way to disable automatic tool window opening before starting it (#30)
- The cookbook will now support also Chef Infra kitchen tests (accepting the license)
- Fixed problem with invalid parameter name when setting Docker integration via socket (#32)

# 1.2.0

- The plugin resource will now check whether the plugin is already installed. Added option for the user to provide
the plugin's directory after extracting - thanks to it the resource will not download the plugin if it does exist. (#23)
- The resources will now throw a Ruby exception rather than ending the Chef run with the Chef::Application.fatal 
mechanism. This means the Chef will show more information about the error. (#25)
- Added support for changing the module's `.iml` file location when using the module resource (#27)

# 1.1.0

- Reworked the Docker integration resource. It allows no more for installing the plugin automatically.
- Added support for setting up code styles on global and project levels. (#16)
- Added support for defining additional attributes for run configuration for some plugins (like Docker integration). (#21)
- Added support for defining the roots config for the SDKs. Since most of the SDK differ between each version, the
resource now no longer tries to set it up automatically and allows overriding the value. (#22)

# 1.0.1

- Fixed the problem with VCS management (#20)
- Fixed error with invalid template when creating a new project (#19)
- Added optionality for the `factory` property of the `intellij_run_configuration` resource and `type` of the `intellij_artifact` resource (#18)

# 1.0.0

- Removed the test cookbook. All the ChefSpec tests were implemented using the `recipe` functionality.
- Added support for creating the shortcut. Removed creating shortcut from the workspace resource.

# 0.4.0

- Updated the API calls for the JetBrains plugins repository. The change in the API made plugins resource to fail.
- Added support for the Docker integration (creating server, adding registry, installing the plugin)
- Added support for managing the Java EE application servers.
- Added support for managing the idea.properties file.
- Added automatic kitchen tests for the master

# 0.3.1

- Fixed the problem with setting invalid attribute for folders within the Run configuration resource. (issue #13).

# 0.3.0

- Added ChefSpec and Kitchen tests for the cookbook
- Fixed error with intellij_module not creating the module correctly
- Added support for the IntelliJ change lists
- Updated README for better displaying information
- Refactored resources to use only `resources` directory - change made to write tests

# 0.2.0

- Added support for the Linux platforms
- Passing the linters (the rubocop and foodcritic from the `delivery local verify`)
- Added TESTING and CHANGELOG to increase the cookbook quality

# 0.1.0

Initial release.

- Added support for resources: artifact, default, module, plugin, project, run_configuration, sdk, workspace

