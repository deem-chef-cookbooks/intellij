describe file('/home/myuser/IntelliJ/product-info.json') do
  it { should exist }
  its('content') { should match /"name"\s*:\s+"IntelliJ IDEA",/ }
  its('content') { should match /"productCode"\s*:\s+"IC",/ }
  its('owner') { should cmp 'myuser' }
  its('group') { should cmp 'mygroup' }
end

describe directory('/home/myuser/IntelliJ/bin') do
  it { should exist }
  its('owner') { should cmp 'myuser' }
  its('group') { should cmp 'mygroup' }
end
