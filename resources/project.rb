resource_name :intellij_project
default_action :create

property :path, String, name_property: true
property :project_name, String
property :default_sdk_name, String
property :default_sdk_type, Symbol, equal_to: [:JavaSDK, :RUBY_SDK], default: :JavaSDK
property :language_level, String, default: 'JDK_10'
property :code_style, String
property :code_style_variables, Hash
property :user, String
property :group, String

action :create do
  args = @new_resource

  directory args.path do
    path args.path
    recursive true
    user args.user
    group args.group
  end

  directory "#{args.path}/.idea" do
    recursive true
    user args.user
    group args.group
  end

  file "#{args.path}/.idea/.name" do
    content args.project_name
    user args.user
    group args.group
    not_if { args.project_name.to_s.empty? }
  end

  if args.default_sdk_name
    file = "#{args.path}/.idea/misc.xml"

    template file do
      source 'project/misc.xml'
      cookbook 'intellij'
      user args.user
      group args.group
      action :create_if_missing
    end

    xml_edit file do
      parent '/project'
      target '/project/component[@name="ProjectRootManager"]'
      fragment get_default_sdk_fragment(args.default_sdk_name, args.default_sdk_type, args.language_level)
      action :append_if_missing
    end
  end

  if args.code_style
    directory "#{args.path}/.idea/codeStyles" do
      recursive true
      user args.user
      group args.group
    end

    code_style_config = "#{args.path}/.idea/codeStyles/codeStyleConfig.xml"

    file code_style_config do
      user args.user
      group args.group
      content '<component name="ProjectCodeStyleConfiguration"><state></state></component>'
      action :create_if_missing
    end

    xml_edit code_style_config do
      parent '/component[@name="ProjectCodeStyleConfiguration"]'
      target '/component[@name="ProjectCodeStyleConfiguration"]/state/option[@name="USE_PER_PROJECT_SETTINGS"]'
      fragment '<option name="USE_PER_PROJECT_SETTINGS" value="true" />'
      action :append_if_missing
    end

    template "#{args.path}/.idea/codeStyles/Project.xml" do
      source args.code_style
      user args.user
      group args.group
      variables args.code_style_variables
    end
  end
end

action :delete do
  args = @new_resource

  directory "#{args.path}/.idea" do
    recursive true
    action :delete
  end
end

action_class do
  include IntelliJ::Helpers
  include IntelliJ::Project
end
