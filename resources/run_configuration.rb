resource_name :intellij_run_configuration
default_action :create

property :project_path, String, required: true
property :config_name, String, name_property: true
property :type, String, required: true
property :factory, String
property :folder, String
property :body, String, required: true
property :user, String
property :group, String
property :attributes, Hash
property :selected, [TrueClass, FalseClass], default: false
property :open_tool_window, [TrueClass, FalseClass], default: true

action :create do
  args = @new_resource

  attributes = {
    name: args.config_name,
    type: args.type,
    factoryName: args.factory,
    folderName: args.folder,
  }.merge(args.attributes || {})

  attributes[:activateToolWindowBeforeRun] = 'false' unless args.open_tool_window

  xml = generate_run_config_xml(attributes, args.body)

  file = "#{args.project_path}/.idea/workspace.xml"

  directory ::File.dirname(file) do
    recursive true
    user args.user
    group args.group
  end

  file file do
    content '<project version="4"></project>'
    user args.user
    group args.group
    action :create_if_missing
  end

  create_missing_component('RunManager', file)

  xml_edit file do
    parent '/project/component[@name="RunManager"]'
    target "/project/component[@name=\"RunManager\"]/configuration[@name=\"#{args.config_name}\"]"
    fragment xml
    action :append_if_missing
  end

  if args.selected
    xml_edit file do
      target '/project/component[@name="RunManager"]'
      fragment lazy { update_manager_selection(file, "#{args.factory}.#{args.config_name}") }
      action :replace
    end
  end
end

action :delete do
  args = @new_resource
  file = "#{args.project_path}/.idea/workspace.xml"

  if ::File.exist?(file)
    xml_edit file do
      target "/project/component[@name=\"RunManager\"]/configuration[@name=\"#{args.config_name}\"]"
      action :remove
    end

    xml_edit file do
      target "/project/component[@name=\"RunManager\"][@selected=\"#{args.factory}.#{args.config_name}\"]/@selected"
      fragment '<selected></selected>'
      action :replace
      not_if { args.factory.to_s.empty? }
    end
  end
end

action_class do
  include IntelliJ::Helpers
  include IntelliJ::Workspace::RunConfiguration
end
